var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const chokidar = require("chokidar");
const jsQR = require("jsqr");
const jimp = require("jimp");
const fs = require("fs");

chokidar.watch("./scan").on("add", (path) => {
  fs.readFile(path, function (err, data) {
    if (err) throw err; // Fail if the file can't be read.
    jimp.read(data, function (err, image) {
      if (err) {
        console.error(err);
      }

      // Creating an instance of qrcode-reader module
      const qrCodeImageArray = new Uint8ClampedArray(image.bitmap.data.buffer);
      const code = jsQR(
        qrCodeImageArray,
        image.bitmap.width,
        image.bitmap.height
      );

      if (code) {
        console.log("Found QR code", code);
      }
    });
  });
});
var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var printRouter = require("./routes/print");

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/print", printRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
