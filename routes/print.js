var express = require("express");
var router = express.Router();
var printer = require("@thiagoelg/node-printer");
var path = require("path");
var fs = require("fs");
const QRCode = require("qrcode");

/* GET users listing. */
router.post("/", function (req, res, next) {
  console.log(printer.getDefaultPrinterName());
  const resultPath = path.join(__dirname, "..", "scan", "test1234.png");

  QRCode.toFile(resultPath, "Some text", function (err) {
    if (err) throw err;
    console.log("done");
    printer.printFile({
      filename: resultPath,
      success: function (jobID) {
        console.log("sent to printer with ID: " + jobID);
      },
      error: function (err) {
        console.log(err);
      },
    });
  });
  res.json({
    status: "ok",
  });
});

module.exports = router;
